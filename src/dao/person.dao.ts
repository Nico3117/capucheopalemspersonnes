import Person from '../models/person.model';
import JobClassPerson from "../models/jobclassperson.model";
import sequelize from "../config/database-connection";
const { Op } = require("sequelize");

export class PersonDao {

    constructor() {
    }

    public async listByRole(role: string) {
        try {
            return await Person.findAll({
                where: {
                    role_id: role,
                }
            });
        } catch(err) {
            console.log(err)
        }
    }

    public async listByNameAndMetier(name: string, metier: string) {
        const [results, metadata] = await sequelize.query(`SELECT p.id, login, nom, prenom, mcp.exp, mc.label
            FROM personne p inner join metier_classe_personne mcp on p.id = mcp.id_personne
            inner join metier_classe mc on mc.id = mcp.id_metier_classe
            where p.role_id = 1 AND ${metier} = mcp.id_metier_classe AND
            (p.nom LIKE '%${name}%' OR p.prenom LIKE '%${name}%');`);
        return results;
    }

    public async listWithLabelMetier() {
        const [results, metadata] = await sequelize.query(`SELECT p.id, mc.id as idMetier, nom, prenom, mcp.exp, mc.label
                    FROM personne p inner join metier_classe_personne mcp on p.id = mcp.id_personne
                    inner join metier_classe mc on mc.id = mcp.id_metier_classe;`);
        return results;
    }

    public async listRole(roleID: string) {
        try {
            return await Person.findAll({
                where: {
                    role_id: roleID
                }
            });
        } catch(err) {
            console.log(err)
        }
    }

    public async create(person) {
        return await Person.create(person);
    }

    public async getByID(personID: string) {
        return await Person.findOne({
            where: {
                id: personID,
            }
        });
    }

    public async delete(personID: string) {
        const person = await this.getByID(personID);
        if (person) {
            await Person.destroy({
                where: {
                    id: personID
                }
            });
            return personID;
        }
    }

    public async update(id, modifiedPerson) {
        const person = await this.getByID(id);
        if (person) {
            await person.update(modifiedPerson)
            await person.save()
            return person
        }
    }
}