import Role from '../models/role.model';
import { where } from 'sequelize/dist';
import Person from '../models/person.model';

export class RoleDao {

    constructor() {
    }

    public async list() {
        try {
            return await Role.findAll();
        } catch(err) {
            console.log(err)
        }
    }
}