import JobClass from '../models/jobclass.model';
import { where } from 'sequelize/dist';

export class JobClassDao {

    constructor() {
    }

    public async list() {
        try {
            return await JobClass.findAll();
        } catch(err) {
            console.log(err)
        }
    }

    public async listPerson(personID: string) {
        try {
            return await JobClass.findAll({
                where: {
                    id_personne: personID
                }
            });
        } catch(err) {
            console.log(err)
        }
    }
}