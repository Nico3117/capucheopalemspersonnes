import JobClassPerson from '../models/jobclassperson.model';
import { where } from 'sequelize/dist';
import JobClass from '../models/jobclass.model';

export class JobClassPersonDao {

    constructor() {
    }

    public async list(job: string) {
        try {
            return await JobClass.findAll({
                where : {
                    label: job
                }
            });
        } catch(err) {
            console.log(err)
        }
    }
}