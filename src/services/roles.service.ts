import { RoleDao } from '../dao/role.dao';

export class RolesService {
    private roleDAO: RoleDao = new RoleDao()

    public async getAllRoles() {
        return await this.roleDAO.list();
    }
}
