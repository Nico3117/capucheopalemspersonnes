import { PersonDao } from '../dao/person.dao';

export class PersonsService {
    private personDAO: PersonDao = new PersonDao()

    public async getAllPersonsByRole(role: string) {
        return await this.personDAO.listByRole(role);
    }

    public async getPersonsByNameOrMetier(name, metier) {
        return await this.personDAO.listByNameAndMetier(name, metier);
    }

    public async getListWithLabelMetier() {
        return await this.personDAO.listWithLabelMetier();
    }

    public async getAllRoles(roleId: string) {
        return await this.personDAO.listRole(roleId);
    }

    public async getPerson(personId: string) {
        const person = await this.personDAO.getByID(personId);
        if (!person) {
            throw new Error('unknown person');
        }
        return person
    }

    public async createPerson(person) {
         return await this.personDAO.create(person);
    }

    public async deletePerson(personId: string) {
        const person = await this.personDAO.getByID(personId);
        if (!person) {
            throw new Error('unknown person');
        }
        return await this.personDAO.delete(personId);
    }

    public async updatePerson(id, person) {
        const existingPerson = await this.personDAO.getByID(id);
        if (!existingPerson) {
            throw new Error('unknow person')
        }
        if (person.role_id) {
            throw new Error('cannot change role_id')
        }
        if (person.login) {
            throw new Error('cannot change login')
        }
        return await this.personDAO.update(id, person)
    }
}
