import { JobClassPersonDao } from '../dao/jobclassperson.dao';

export class JobClassPersonService {
    private jobclasspersonDAO: JobClassPersonDao = new JobClassPersonDao()

    public async getPersonByClass(job: string) {
        return await this.jobclasspersonDAO.list(job);
    }
}
