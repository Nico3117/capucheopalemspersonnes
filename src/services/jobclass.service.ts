import { JobClassDao } from '../dao/jobclass.dao';

export class JobClassService {
    private jobclassDAO: JobClassDao = new JobClassDao()

    public async getAllClass() {
        return await this.jobclassDAO.list();
    }

    public async getAllPerson(personID: string) {
        return await this.jobclassDAO.listPerson(personID);
    }
}
