import { Router } from 'express';
import { JobClassPersonService } from '../services/jobclassperson.service';

const jobclasspersonController = Router();
const jobclasspersonService = new JobClassPersonService();

jobclasspersonController.get('/:job', async (req, res) => {
    const jobclasspersons = await jobclasspersonService.getPersonByClass(req.params.job);
    const promise = jobclasspersons.map((person) => {
        return {
            id: person.id,
            name: person.nom + '_' + person.prenom + '_' + person.login
        }
    })
    const results = await Promise.all(promise);
    res.status(200).send(results);
})

export default jobclasspersonController;
