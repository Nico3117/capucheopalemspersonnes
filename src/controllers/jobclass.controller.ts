import { Router } from 'express';
import { JobClassService } from '../services/jobclass.service';

const jobclassController = Router();
const jobclassService = new JobClassService();

 jobclassController.get('/', async (req, res) => {
    const jobclass = await jobclassService.getAllClass();
    res.status(200).send(jobclass);
})

jobclassController.get('/:personID', async (req, res) => {
    const jobclass = await jobclassService.getAllPerson(req.params.personID);
    res.status(200).send(jobclass);
})

export default jobclassController;
