import { Router } from 'express';
import { RolesService } from '../services/roles.service';

const roleController = Router();
const rolesService = new RolesService();

roleController.get('/', async (req, res) => {
    const roles = await rolesService.getAllRoles();
    res.status(200).send(roles);
})

export default roleController;
