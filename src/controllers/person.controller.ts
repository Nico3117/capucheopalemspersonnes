import { Router } from 'express';
import { PersonsService } from '../services/persons.service';

const personController = Router();
const personsService = new PersonsService();

/**
 * @openapi
 * /api/personne/getByNameAndJob:
 *   get:
 *     summary: Retrieve a list based on nom/prenom or metier_id
 *     tags:
 *        - Personnage
 *     parameters:
 *        - name: name
 *          in: query
 *          required: true
 *          description: The name of the adventurer to return
 *          schema:
 *              type: string
 *        - name: metierID
 *          in: query
 *          required: true
 *          description: The metier_id of the adventurer to return, if metierID is null the default value is 1.
 *          schema:
 *              type: integer
 *     responses:
 *       '200':
 *        description: OK
 *
 */

 personController.get('/getByNameAndJob', async (req, res) => {
        let name = req.query.name;
        let metier = req.query.metierID;
        if(name === undefined){
            name = "";
        }
        if(metier === undefined){
            metier = "1";
        }
        const persons = await personsService.getPersonsByNameOrMetier(name, metier);
        const promise = await Promise.all(persons.map((person) => {
            return {
                id: person.id,
                nom: person.nom,
                prenom: person.prenom,
                login : person.login,
                classe : {
                    id : metier,
                    label : person.label,
                    exp : person.exp
                }
            }
        }))
        const results = await Promise.all(promise);
        res.status(200).send(results);
})

/**
 * @openapi
 * /api/personne/getLabelMetier:
 *   get:
 *      summary: Get adventurer with metier and exp
 *      tags:
 *        - Personnage
 *      responses:
 *       '200':
 *        description: OK
 */

personController.get('/getLabelMetier', async (req, res) => {
        const persons = await personsService.getListWithLabelMetier();
        const promise = await Promise.all(persons.map((person) => {
            return {
                id: person.id,
                nom: person.nom,
                prenom : person.prenom,
                classe : {
                    id : person.idMetier,
                    label : person.label,
                    exp : person.exp
                }
            }
        }))
        const results = await Promise.all(promise);
        res.status(200).send(results);
})

/**
 * @openapi
 * /api/personne/{role_id}:
 *   get:
 *     summary: Retrieve a list based on role_id
 *     tags:
 *        - Role
 *     parameters:
 *        - name: role_id
 *          in: path
 *          required: true
 *          description: The id of the role to return
 *          schema:
 *              type: integer
 *     responses:
 *       '200':
 *        description: OK
 *
 */

personController.get('/:role_id', async (req, res) => {
        const role = req.params.role_id;
        const persons = await personsService.getAllPersonsByRole(role);
        const promise = persons.map((person) => {
            return {
                id: person.id,
                name: person.nom + '_' + person.prenom + '_' + person.login
            }
        })
        const results = await Promise.all(promise);
        res.status(200).send(results);
})




export default personController;
