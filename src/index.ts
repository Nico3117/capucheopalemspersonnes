import express from 'express';
import jobclassController from './controllers/jobclass.controller';
import jobclasspersonController from './controllers/jobclassperson.controller';
import personController from "./controllers/person.controller";
import roleController from './controllers/role.controller';
const cors = require('cors');
const app = express();

const port = process.env.PORT || 3000; // default port to listen
const dotenv = require('dotenv')

dotenv.config();

app.use(cors());

// middleware used to parse incoming requests with JSON payloads
app.use(express.json());

app.use('/api/personne', personController);
app.use('/api/role', roleController);
app.use('/api/classe', jobclasspersonController);

// swagger configuration
const swaggerJSDoc = require('swagger-jsdoc');

const swaggerDefinition: any = {
    openapi: '3.0.0',
    info: {
        title: 'API REST Microservice Personne',
        version: '1.0.0',
        description: 'This is REST API for microservice personne'
    },
    components: {
        securitySchemes: {
            jwt: {
                type: 'http',
                name: 'Authorization',
                scheme: 'bearer',
                in: 'header',
                bearerFormat: 'JWT',
            }
        }
    },

    security: [{
        jwt: []
    }],
    servers: [
        {
            url: `http://localhost:${port}`,
            description: 'Local server'
        }
    ]
};

const options = {
    swaggerDefinition,
    // Paths to files containing OpenAPI definitions
    apis: ['**/controllers/*.js', './controllers/*.ts'],
};

const swaggerSpec = swaggerJSDoc(options);
const swaggerUi = require('swagger-ui-express');

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.get('/swagger.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.send(swaggerSpec)
});

// start the Express server
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
