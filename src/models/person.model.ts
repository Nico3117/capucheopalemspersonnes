const { DataTypes } = require("sequelize");
import databaseConnection from "../config/database-connection";
import Role from '../models/role.model'
import JobClassPerson from "./jobclassperson.model";

const Person = databaseConnection.define("Personne",{
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  nom: {
    type: DataTypes.STRING
  },
  prenom: {
    type: DataTypes.STRING
  },
  login: {
    type: DataTypes.STRING
  },
  mot_de_passe: {
    type: DataTypes.STRING
  },
  argent: {
    type: DataTypes.STRING
  },
  role_id: {
    type: DataTypes.STRING
  },
}, {
  tableName: 'personne',
  timestamps: false
});



(async () => {
  try {
    await databaseConnection.sync({ force: false });
  } catch (err) {
  console.log(err)
  }})();

export default Person
