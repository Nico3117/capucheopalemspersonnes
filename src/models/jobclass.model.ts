const { DataTypes } = require("sequelize");
import databaseConnection from "../config/database-connection";

const JobClass = databaseConnection.define("Metier Classe",{
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  label: {
    type: DataTypes.INTEGER
  }
}, {
  tableName: 'metier_classe',
  timestamps: false
});

(async () => {
  try {
    await databaseConnection.sync({ force: false });
  } catch (err) {
  console.log(err)
  }})();

export default JobClass
