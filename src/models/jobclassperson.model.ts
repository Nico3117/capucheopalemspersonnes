const { DataTypes } = require("sequelize");
import databaseConnection from "../config/database-connection";
import Person from "./person.model";

const JobClassPerson = databaseConnection.define("MetierClassePersonne",{
  id_metier_classe: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  id_personne: {
    type: DataTypes.INTEGER,
    primaryKey: true
  },
  exp: {
    type: DataTypes.STRING
  },
}, {
  tableName: 'metier_classe_personne',
  timestamps: false
});


(async () => {
  try {
    await databaseConnection.sync({ force: false });
  } catch (err) {
    console.log(err)
  }
})();

export default JobClassPerson
