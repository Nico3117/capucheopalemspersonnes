const { DataTypes } = require("sequelize");
import databaseConnection from "../config/database-connection";
import Person from "./person.model";

const Role = databaseConnection.define("Role",{
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  label: {
    type: DataTypes.STRING,
    defaultValue: 'aventurier'
  },
}, {
  tableName: 'role',
  timestamps: false
});

(async () => {
  try {
    await databaseConnection.sync({ force: false });
  } catch (err) {
  console.log(err)
  }})();

export default Role
